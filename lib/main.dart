import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int value = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AppBar'),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.black12,
        child: Column(
          children: [
            Text(
              '${value}',
              style: TextStyle(
                fontSize: 40,
              ),
            ),
            Row(
              children: [
                Expanded(child: Container()),
                Container(
                  padding: EdgeInsets.all(50),
                  child: ElevatedButton(
                    onPressed: () {
                      setState(() => value--);
                    },
                    child: Text('-'),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(50),
                  child: ElevatedButton(
                    onPressed: () {
                      setState(() => value++);
                    },
                    child: Text('+'),
                  ),
                ),
                Expanded(child: Container()),
              ],
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => PageTwo()),
                );
              },
              child: Text('next page'),
            ),
          ],
        ),
      ),
    );
  }
}

class PageTwo extends StatelessWidget {
  int value = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PageTwo'),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.orange[200],
        child: Column(
          children: [
            SizedBox(
              height: 30,
            ),
            Container(
              height: 600,
              width: 400,
              child: ListView(
                children: [
                  Image.network('https://vypechka-online.ru/wp-content/uploads/2019/09/0SXAklSfD7E.jpg'),
                  SizedBox(
                    height: 50,
                  ),
                  Image.network(
                      'http://pristor.ru/wp-content/uploads/2018/09/%D0%9A%D1%80%D0%B0%D1%81%D0%B8%D0%B2%D1%8B%D0%B5-%D0%B8-%D0%BF%D1%80%D0%B8%D0%BA%D0%BE%D0%BB%D1%8C%D0%BD%D1%8B%D0%B5-%D0%BE%D0%B1%D0%BE%D0%B8-%D0%BD%D0%B0-%D1%82%D0%B5%D0%BB%D0%B5%D1%84%D0%BE%D0%BD-%D0%94%D0%BE%D1%80%D0%BE%D0%B3%D0%B0-%D0%BF%D1%83%D1%82%D0%B5%D1%88%D0%B5%D1%81%D1%82%D0%B2%D0%B8%D0%B5-12.jpg'),
                ],
              ),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pop(
                  context,
                  MaterialPageRoute(builder: (context) => MyHomePage()),
                );
              },
              child: Text('previous page'),
            ),
          ],
        ),
      ),
    );
  }
}
